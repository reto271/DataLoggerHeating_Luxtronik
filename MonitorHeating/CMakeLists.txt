cmake_minimum_required(VERSION 3.13.4)
project(MonitorHeating)

enable_testing()

# Define compiler switches
set(CMAKE_CXX_FLAGS "-ggdb -g3 -Wall -std=c++17")

# This sets the output dir to /bin
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

include_directories(${CMAKE_SOURCE_DIR}/..)
add_executable(MonitorHeating
                src/MonitorHeating_main.cpp
                src/TcpConnection.cpp
                src/ReceiveDataBuffer.cpp
                src/ValueResponse.cpp
                src/SynchronizeTime.cpp
                ../Common/src/BitBuffer.cpp
                ../Common/src/IP_AddressValidator.cpp
                ../Common/src/ProgramVersion.cpp
                ../Common/src/ValueTableCommon.cpp
                ../Common/src/ValueTable.cpp)

set_target_properties(MonitorHeating
    PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS ON)

target_link_libraries(MonitorHeating)
