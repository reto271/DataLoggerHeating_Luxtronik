#!/bin/bash

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null

SW_Version=$(cat ../../VersionNumber.txt | \
                        awk -F'"' '{ print $2 }')

#echo ""
echo "SW Version: ${SW_Version}"
#echo "---"

export DATA_LOGGER_LUXTRONIK_SW_VERSION="${SW_Version}"

# Back to the original location
popd > /dev/null
