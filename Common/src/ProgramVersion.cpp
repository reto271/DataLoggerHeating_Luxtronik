#include "ProgramVersion.hpp"

// Reads the version number. The version number must be in quotes
const std::string VersionString =
#include "../../VersionNumber.txt"
    ;

ProgramVersion::ProgramVersion()
{
}

void ProgramVersion::printProgramInfo(std::string programName)
{
    std::cout << "-----------------------------------------------------" << std::endl;
    std::cout << " Program: '" << programName << "'" << std::endl;
    std::cout << " Version: " << VersionString << std::endl;
    if (std::string::npos == VersionString.find('B')) {
        std::cout << " Status: released" << std::endl;
    } else {
        std::cout << " Beta Version" << std::endl;
    }
    std::cout << "-----------------------------------------------------" << std::endl;
}
