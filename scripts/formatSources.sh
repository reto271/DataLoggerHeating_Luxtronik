#!/bin/bash

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null

echo "Format sources: MonitorHeating"
../MonitorHeating/scripts/formatSources.sh
feedback=$?

echo "Format sources: DataDecoder"
if [ 0 -eq ${feedback} ] ; then
    ../DataDecoder/scripts/formatSources.sh
    feedback=$?
fi

echo "Format sources: Common"
if [ 0 -eq ${feedback} ] ; then
    ../Common/scripts/formatSources.sh
    feedback=$?
fi

echo "Format sources: Test"
if [ 0 -eq ${feedback} ] ; then
    ../Test/scripts/formatSources.sh
    feedback=$?
fi

echo "Format sources: Utils"
if [ 0 -eq ${feedback} ] ; then
    ../Utils/scripts/formatSources.sh
    feedback=$?
fi

# Back to the original location
popd > /dev/null

exit ${feedback}
