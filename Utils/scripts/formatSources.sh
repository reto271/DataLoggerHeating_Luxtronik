#!/bin/bash

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ..

../Common/scripts/showSW_Version.sh

if [ -e ../.clang-format ] ; then
    # Without excluding directories
    codeFolder=src
    format_files=$(find "${codeFolder}" -type f)

    # Exclude some directories
    # codeFolder=src
    # exclude_codeFolder=./submodule
    # format_files=`find "${codeFolder}" -type f -path "${exclude_codeFolder}" -prune

    for file in $format_files
    do
        clang-format --style=file --verbose -i "$file"
    done
else
    echo "There is no .clang-format in the project root"
    echo ""
fi

# Back to the original location
popd > /dev/null

exit 0
